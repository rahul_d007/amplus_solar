-- MySQL dump 10.13  Distrib 8.0.26, for Linux (x86_64)
--
-- Host: localhost    Database: amplus_solar_db
-- ------------------------------------------------------
-- Server version	8.0.26

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `auth_group`
--

DROP TABLE IF EXISTS `auth_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `auth_group` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group`
--

LOCK TABLES `auth_group` WRITE;
/*!40000 ALTER TABLE `auth_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_group_permissions`
--

DROP TABLE IF EXISTS `auth_group_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `auth_group_permissions` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `group_id` int NOT NULL,
  `permission_id` int NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_group_permissions_group_id_permission_id_0cd325b0_uniq` (`group_id`,`permission_id`),
  KEY `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` (`permission_id`),
  CONSTRAINT `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_group_permissions_group_id_b120cbf9_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group_permissions`
--

LOCK TABLES `auth_group_permissions` WRITE;
/*!40000 ALTER TABLE `auth_group_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_permission`
--

DROP TABLE IF EXISTS `auth_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `auth_permission` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `content_type_id` int NOT NULL,
  `codename` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_permission_content_type_id_codename_01ab375a_uniq` (`content_type_id`,`codename`),
  CONSTRAINT `auth_permission_content_type_id_2f476e4b_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_permission`
--

LOCK TABLES `auth_permission` WRITE;
/*!40000 ALTER TABLE `auth_permission` DISABLE KEYS */;
INSERT INTO `auth_permission` VALUES (1,'Can add log entry',1,'add_logentry'),(2,'Can change log entry',1,'change_logentry'),(3,'Can delete log entry',1,'delete_logentry'),(4,'Can view log entry',1,'view_logentry'),(5,'Can add permission',2,'add_permission'),(6,'Can change permission',2,'change_permission'),(7,'Can delete permission',2,'delete_permission'),(8,'Can view permission',2,'view_permission'),(9,'Can add group',3,'add_group'),(10,'Can change group',3,'change_group'),(11,'Can delete group',3,'delete_group'),(12,'Can view group',3,'view_group'),(13,'Can add user',4,'add_user'),(14,'Can change user',4,'change_user'),(15,'Can delete user',4,'delete_user'),(16,'Can view user',4,'view_user'),(17,'Can add content type',5,'add_contenttype'),(18,'Can change content type',5,'change_contenttype'),(19,'Can delete content type',5,'delete_contenttype'),(20,'Can view content type',5,'view_contenttype'),(21,'Can add session',6,'add_session'),(22,'Can change session',6,'change_session'),(23,'Can delete session',6,'delete_session'),(24,'Can view session',6,'view_session'),(25,'Can add solar plant',7,'add_solarplant'),(26,'Can change solar plant',7,'change_solarplant'),(27,'Can delete solar plant',7,'delete_solarplant'),(28,'Can view solar plant',7,'view_solarplant');
/*!40000 ALTER TABLE `auth_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user`
--

DROP TABLE IF EXISTS `auth_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `auth_user` (
  `id` int NOT NULL AUTO_INCREMENT,
  `password` varchar(128) NOT NULL,
  `last_login` datetime(6) DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(150) NOT NULL,
  `first_name` varchar(150) NOT NULL,
  `last_name` varchar(150) NOT NULL,
  `email` varchar(254) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user`
--

LOCK TABLES `auth_user` WRITE;
/*!40000 ALTER TABLE `auth_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_groups`
--

DROP TABLE IF EXISTS `auth_user_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `auth_user_groups` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL,
  `group_id` int NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_groups_user_id_group_id_94350c0c_uniq` (`user_id`,`group_id`),
  KEY `auth_user_groups_group_id_97559544_fk_auth_group_id` (`group_id`),
  CONSTRAINT `auth_user_groups_group_id_97559544_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  CONSTRAINT `auth_user_groups_user_id_6a12ed8b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_groups`
--

LOCK TABLES `auth_user_groups` WRITE;
/*!40000 ALTER TABLE `auth_user_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_user_permissions`
--

DROP TABLE IF EXISTS `auth_user_user_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `auth_user_user_permissions` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL,
  `permission_id` int NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_user_permissions_user_id_permission_id_14a6b632_uniq` (`user_id`,`permission_id`),
  KEY `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` (`permission_id`),
  CONSTRAINT `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_user_permissions`
--

LOCK TABLES `auth_user_user_permissions` WRITE;
/*!40000 ALTER TABLE `auth_user_user_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_user_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_admin_log`
--

DROP TABLE IF EXISTS `django_admin_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `django_admin_log` (
  `id` int NOT NULL AUTO_INCREMENT,
  `action_time` datetime(6) NOT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint unsigned NOT NULL,
  `change_message` longtext NOT NULL,
  `content_type_id` int DEFAULT NULL,
  `user_id` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `django_admin_log_content_type_id_c4bce8eb_fk_django_co` (`content_type_id`),
  KEY `django_admin_log_user_id_c564eba6_fk_auth_user_id` (`user_id`),
  CONSTRAINT `django_admin_log_content_type_id_c4bce8eb_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  CONSTRAINT `django_admin_log_user_id_c564eba6_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `django_admin_log_chk_1` CHECK ((`action_flag` >= 0))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_admin_log`
--

LOCK TABLES `django_admin_log` WRITE;
/*!40000 ALTER TABLE `django_admin_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `django_admin_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_content_type`
--

DROP TABLE IF EXISTS `django_content_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `django_content_type` (
  `id` int NOT NULL AUTO_INCREMENT,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `django_content_type_app_label_model_76bd3d3b_uniq` (`app_label`,`model`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_content_type`
--

LOCK TABLES `django_content_type` WRITE;
/*!40000 ALTER TABLE `django_content_type` DISABLE KEYS */;
INSERT INTO `django_content_type` VALUES (1,'admin','logentry'),(3,'auth','group'),(2,'auth','permission'),(4,'auth','user'),(5,'contenttypes','contenttype'),(7,'plant_management','solarplant'),(6,'sessions','session');
/*!40000 ALTER TABLE `django_content_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_migrations`
--

DROP TABLE IF EXISTS `django_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `django_migrations` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_migrations`
--

LOCK TABLES `django_migrations` WRITE;
/*!40000 ALTER TABLE `django_migrations` DISABLE KEYS */;
INSERT INTO `django_migrations` VALUES (1,'contenttypes','0001_initial','2021-09-23 08:07:10.343626'),(2,'auth','0001_initial','2021-09-23 08:07:10.957992'),(3,'admin','0001_initial','2021-09-23 08:07:11.127032'),(4,'admin','0002_logentry_remove_auto_add','2021-09-23 08:07:11.149779'),(5,'admin','0003_logentry_add_action_flag_choices','2021-09-23 08:07:11.176254'),(6,'contenttypes','0002_remove_content_type_name','2021-09-23 08:07:11.351088'),(7,'auth','0002_alter_permission_name_max_length','2021-09-23 08:07:11.426667'),(8,'auth','0003_alter_user_email_max_length','2021-09-23 08:07:11.480173'),(9,'auth','0004_alter_user_username_opts','2021-09-23 08:07:11.502762'),(10,'auth','0005_alter_user_last_login_null','2021-09-23 08:07:11.577349'),(11,'auth','0006_require_contenttypes_0002','2021-09-23 08:07:11.583764'),(12,'auth','0007_alter_validators_add_error_messages','2021-09-23 08:07:11.608082'),(13,'auth','0008_alter_user_username_max_length','2021-09-23 08:07:11.671096'),(14,'auth','0009_alter_user_last_name_max_length','2021-09-23 08:07:11.729456'),(15,'auth','0010_alter_group_name_max_length','2021-09-23 08:07:11.771426'),(16,'auth','0011_update_proxy_permissions','2021-09-23 08:07:11.791749'),(17,'auth','0012_alter_user_first_name_max_length','2021-09-23 08:07:11.871101'),(18,'plant_management','0001_initial','2021-09-23 08:07:11.906396'),(19,'sessions','0001_initial','2021-09-23 08:07:11.960092');
/*!40000 ALTER TABLE `django_migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_session`
--

DROP TABLE IF EXISTS `django_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime(6) NOT NULL,
  PRIMARY KEY (`session_key`),
  KEY `django_session_expire_date_a5c62663` (`expire_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_session`
--

LOCK TABLES `django_session` WRITE;
/*!40000 ALTER TABLE `django_session` DISABLE KEYS */;
/*!40000 ALTER TABLE `django_session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `plants`
--

DROP TABLE IF EXISTS `plants`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `plants` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `plant_id` int NOT NULL,
  `date` date NOT NULL,
  `parameter` varchar(30) NOT NULL,
  `value` double NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=187 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `plants`
--

LOCK TABLES `plants` WRITE;
/*!40000 ALTER TABLE `plants` DISABLE KEYS */;
INSERT INTO `plants` VALUES (1,501,'2021-03-31','generation',1613.87000000011),(2,501,'2021-03-30','generation',1464.57999999984),(3,501,'2021-03-29','generation',1480.44999999995),(4,501,'2021-03-28','generation',1443.84000000008),(5,501,'2021-03-27','generation',1469.43999999994),(6,501,'2021-03-26','generation',1629.56000000006),(7,501,'2021-03-25','generation',1485.32000000007),(8,501,'2021-03-24','generation',1457.65999999992),(9,501,'2021-03-23','generation',1148.03000000003),(10,501,'2021-03-22','generation',1263.87000000011),(11,501,'2021-03-21','generation',1208.31999999983),(12,501,'2021-03-20','generation',1477),(13,501,'2021-03-19','generation',1124.60000000009),(14,501,'2021-03-18','generation',1111.04000000004),(15,501,'2021-03-17','generation',1150.46999999997),(16,501,'2021-03-16','generation',1358.32999999984),(17,501,'2021-03-15','generation',1383.43000000017),(18,501,'2021-03-14','generation',1472.8899999999),(19,501,'2021-03-13','generation',1571.71999999997),(20,501,'2021-03-12','generation',283.130000000121),(21,501,'2021-03-11','generation',1362.31000000006),(22,501,'2021-03-10','generation',1380.59999999986),(23,501,'2021-03-09','generation',1314.82000000007),(24,501,'2021-03-08','generation',1070.71999999997),(25,501,'2021-03-07','generation',1221.37999999989),(26,501,'2021-03-06','generation',1421.18000000017),(27,501,'2021-03-05','generation',1263.73999999999),(28,501,'2021-03-04','generation',1509.25),(29,501,'2021-03-03','generation',1597.57000000007),(30,501,'2021-03-02','generation',1606.90999999992),(31,501,'2021-03-01','generation',1415.55000000005),(32,502,'2021-03-31','irradiation',7866.94846838325),(33,502,'2021-03-31','generation',2158.46999999997),(34,502,'2021-03-30','irradiation',6409.55379452506),(35,502,'2021-03-30','generation',1780.47999999998),(36,502,'2021-03-29','irradiation',5864.68803852022),(37,502,'2021-03-29','generation',1681.92000000016),(38,502,'2021-03-28','irradiation',6371.66893942111),(39,502,'2021-03-28','generation',1791.60999999987),(40,502,'2021-03-27','irradiation',6273.90138132035),(41,502,'2021-03-27','generation',1755.39000000013),(42,502,'2021-03-26','irradiation',6807.79388917995),(43,502,'2021-03-26','generation',1887.23999999999),(44,502,'2021-03-25','irradiation',6152.0398197547),(45,502,'2021-03-25','generation',1769.33999999985),(46,502,'2021-03-24','irradiation',6665.12651036003),(47,502,'2021-03-24','generation',1650.05000000005),(48,502,'2021-03-23','irradiation',5814.75996991678),(49,502,'2021-03-23','generation',350.719999999972),(50,502,'2021-03-22','irradiation',3224.59945760591),(51,502,'2021-03-22','generation',979.969999999972),(52,502,'2021-03-21','irradiation',1314.72729503826),(53,502,'2021-03-21','generation',309.760000000009),(54,502,'2021-03-20','irradiation',6134.39423061989),(55,502,'2021-03-20','generation',1084.41000000015),(56,502,'2021-03-19','irradiation',6015.38647121859),(57,502,'2021-03-19','generation',1577.72999999998),(58,502,'2021-03-18','irradiation',4853.30476062093),(59,502,'2021-03-18','generation',1196.15999999992),(60,502,'2021-03-17','irradiation',5282.89374577682),(61,502,'2021-03-17','generation',1194.62000000011),(62,502,'2021-03-16','irradiation',5064.60229326061),(63,502,'2021-03-16','generation',1215.22999999998),(64,502,'2021-03-15','irradiation',3912.22519368085),(65,502,'2021-03-15','generation',449.030000000028),(66,502,'2021-03-14','irradiation',5551.71770051122),(67,502,'2021-03-14','generation',1475.83999999985),(68,502,'2021-03-13','irradiation',4688.03594212296),(69,502,'2021-03-13','generation',1320.95999999996),(70,502,'2021-03-12','irradiation',2832.44831606452),(71,502,'2021-03-12','generation',720.760000000009),(72,502,'2021-03-11','irradiation',5605.78841231548),(73,502,'2021-03-11','generation',1327.88000000012),(74,502,'2021-03-10','irradiation',5453.44588651181),(75,502,'2021-03-10','generation',1492.08999999985),(76,502,'2021-03-09','irradiation',5008.81761649864),(77,502,'2021-03-09','generation',1304.7100000002),(78,502,'2021-03-08','irradiation',5312.60864512707),(79,502,'2021-03-08','generation',624.119999999879),(80,502,'2021-03-07','irradiation',2030.77235479061),(81,502,'2021-03-07','generation',595.459999999963),(82,502,'2021-03-06','irradiation',5646.30482658339),(83,502,'2021-03-06','generation',1191.30000000005),(84,502,'2021-03-05','irradiation',6274.54369726471),(85,502,'2021-03-05','generation',1548.54000000004),(86,502,'2021-03-04','irradiation',5284.41577359781),(87,502,'2021-03-04','generation',1469.94999999995),(88,502,'2021-03-03','irradiation',5354.2286979311),(89,502,'2021-03-03','generation',1589.89000000013),(90,502,'2021-03-02','irradiation',6551.04429563827),(91,502,'2021-03-02','generation',1657.72999999998),(92,502,'2021-03-01','irradiation',5822.31202278298),(93,502,'2021-03-01','generation',474.489999999991),(94,503,'2021-03-31','irradiation',7634.8784647059),(95,503,'2021-03-31','generation',1703.75),(96,503,'2021-03-30','irradiation',6324.273807352955),(97,503,'2021-03-30','generation',1676.3699999999953),(98,503,'2021-03-29','irradiation',5230.174764705894),(99,503,'2021-03-29','generation',1452.8199999999488),(100,503,'2021-03-28','irradiation',6005.066982352956),(101,503,'2021-03-28','generation',1618.75),(102,503,'2021-03-27','irradiation',6551.594113235308),(103,503,'2021-03-27','generation',1752.8700000001118),(104,503,'2021-03-26','irradiation',6936.814747058841),(105,503,'2021-03-26','generation',1878.1899999999441),(106,503,'2021-03-25','irradiation',6434.086698529428),(107,503,'2021-03-25','generation',1647.5),(108,503,'2021-03-24','irradiation',6756.828935294133),(109,503,'2021-03-24','generation',1768.0600000000559),(110,503,'2021-03-23','irradiation',2013.7296838235336),(111,503,'2021-03-23','generation',307.0699999999488),(112,503,'2021-03-22','irradiation',3060.2999514705957),(113,503,'2021-03-22','generation',890.5599999999395),(114,503,'2021-03-21','irradiation',5630.846982352955),(115,503,'2021-03-21','generation',1479.75),(116,503,'2021-03-20','irradiation',6518.959927941191),(117,503,'2021-03-20','generation',1684.0600000000559),(118,503,'2021-03-19','irradiation',5024.546377941188),(119,503,'2021-03-19','generation',1242.1300000000047),(120,503,'2021-03-18','irradiation',5919.379107352955),(121,503,'2021-03-18','generation',1347.3099999999395),(122,503,'2021-03-17','irradiation',5909.292344117661),(123,503,'2021-03-17','generation',1556.9300000000512),(124,503,'2021-03-16','irradiation',5719.350679411778),(125,503,'2021-03-16','generation',647),(126,503,'2021-03-15','irradiation',5590.69587794119),(127,503,'2021-03-15','generation',1505.1300000000047),(128,503,'2021-03-14','irradiation',0),(129,503,'2021-03-14','generation',1767.0600000000559),(130,503,'2021-03-13','irradiation',4893.410951470601),(131,503,'2021-03-13','generation',1342.8099999999395),(132,503,'2021-03-12','irradiation',1990.2250323529454),(133,503,'2021-03-12','generation',597.8199999999488),(134,503,'2021-03-11','irradiation',5988.087833823544),(135,503,'2021-03-11','generation',1578.75),(136,503,'2021-03-10','irradiation',4713.668616176482),(137,503,'2021-03-10','generation',1275.75),(138,503,'2021-03-09','irradiation',5516.6815323529545),(139,503,'2021-03-09','generation',1488.0600000000559),(140,503,'2021-03-08','irradiation',5385.158377941189),(141,503,'2021-03-08','generation',400.9399999999441),(142,503,'2021-03-07','irradiation',4048.1723779411864),(143,503,'2021-03-07','generation',1106.75),(144,503,'2021-03-06','irradiation',6106.439911764722),(145,503,'2021-03-06','generation',1205.4300000000512),(146,503,'2021-03-05','irradiation',6334.873372058839),(147,503,'2021-03-05','generation',1718.1300000000047),(148,503,'2021-03-04','irradiation',6072.947722058838),(149,503,'2021-03-04','generation',1645.3100000000559),(150,503,'2021-03-03','irradiation',6464.064322058839),(151,503,'2021-03-03','generation',1229.3199999999488),(152,503,'2021-03-02','irradiation',6437.305257352957),(153,503,'2021-03-02','generation',1755.5599999999395),(154,503,'2021-03-01','irradiation',6351.2613397058985),(155,503,'2021-03-01','generation',1747.4400000000605),(156,501,'2021-03-31','irradiation',0),(157,501,'2021-03-30','irradiation',0),(158,501,'2021-03-29','irradiation',0),(159,501,'2021-03-28','irradiation',0),(160,501,'2021-03-27','irradiation',0),(161,501,'2021-03-26','irradiation',0),(162,501,'2021-03-25','irradiation',0),(163,501,'2021-03-24','irradiation',0),(164,501,'2021-03-23','irradiation',0),(165,501,'2021-03-22','irradiation',0),(166,501,'2021-03-21','irradiation',0),(167,501,'2021-03-20','irradiation',0),(168,501,'2021-03-19','irradiation',0),(169,501,'2021-03-18','irradiation',0),(170,501,'2021-03-17','irradiation',0),(171,501,'2021-03-16','irradiation',0),(172,501,'2021-03-15','irradiation',0),(173,501,'2021-03-14','irradiation',0),(174,501,'2021-03-13','irradiation',0),(175,501,'2021-03-12','irradiation',0),(176,501,'2021-03-11','irradiation',0),(177,501,'2021-03-10','irradiation',0),(178,501,'2021-03-09','irradiation',168.933487026002),(179,501,'2021-03-08','irradiation',4774.326642447665),(180,501,'2021-03-07','irradiation',5691.560580378992),(181,501,'2021-03-06','irradiation',6561.692866262677),(182,501,'2021-03-05','irradiation',6379.5276430607455),(183,501,'2021-03-04','irradiation',5997.638100222163),(184,501,'2021-03-03','irradiation',6630.930630403383),(185,501,'2021-03-02','irradiation',6658.392724353423),(186,501,'2021-03-01','irradiation',6123.131547727193);
/*!40000 ALTER TABLE `plants` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-09-24 18:22:43
