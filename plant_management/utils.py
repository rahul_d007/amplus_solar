from rest_framework.exceptions import APIException


class CustomAPIException(APIException):
    status_code = 403
