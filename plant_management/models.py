from django.db import models


# Create your models here.

class SolarPlant(models.Model):
    PARAMETER_CHOICES = (
        ("generation", "Generation"),
        ("irradiation)", "Irradiation)"),
    )
    plant_id = models.IntegerField(default=0)
    date = models.DateField()
    parameter = models.CharField(max_length=30, choices=PARAMETER_CHOICES, default="generation")
    value = models.FloatField()

    
    class Meta:
        db_table = "plants"
