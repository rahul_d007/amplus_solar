import csv
import io

from rest_framework import serializers
from plant_management.models import SolarPlant


class CreatePlantSerializer(serializers.Serializer):
    file = serializers.FileField(write_only=True)

    class Meta:
        fields = ('file','id')


# class PlantSerializers(serializers.ModelSerializer):
#
#     class Meta:
#         model = SolarPlantData
#         fields = "__all__"


class FilteredPlantSerializers(serializers.ModelSerializer):
    # parameters = serializers.SerializerMethodField()
    # date = serializers.SerializerMethodField()
    date = serializers.CharField(source='date.date')
    plant_id = serializers.CharField(source='plant.plant_id')
    data = serializers.SerializerMethodField()


    class Meta:
        model = SolarPlant
        fields = ("plant_id","date","parameter","value")
    
    def get_data(self,instance):
        result = []
        qs = instance.plant_data.values("date")
        for i in qs:
            dict_data={i.parameter:i.value}
            result.append(dict_data)

        return result
    #