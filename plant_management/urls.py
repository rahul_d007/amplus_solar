# Local Imports
from .views import SolarPlantViews, HomeView, PlantwiseGraph
# Django Imports
from django.urls import path



urlpatterns = [
    path('plants', SolarPlantViews.as_view(), name='soloar_plants'),
    path('index', HomeView.as_view(), name='index'),
    path('display/graph', PlantwiseGraph.as_view(), name='filter_value'),

]
