import csv
import io

from django.views import View
from django.shortcuts import render


from rest_framework import status
from rest_framework.generics import ListCreateAPIView
from rest_framework.response import Response

from .models import SolarPlant
from django_filters.rest_framework import DjangoFilterBackend

from django.http import HttpResponse
from django.template.loader import get_template
from .utils import CustomAPIException


class HomeView(View):
    def get(self, request):
        solar_plant = SolarPlant.objects.all()
        solar_plant_ids = list(set(solar_plant.values_list("plant_id",flat=True)))
        solar_plant_dates =solar_plant.values_list("date", flat=True )
        start_date,end_date =  min(solar_plant_dates),max(solar_plant_dates)
        context = {
             "min_date": start_date,
             "max_date" : end_date,
             "plant_ids": solar_plant_ids
        }
        print(context)
        return render(request, 'index.html',context=context)

class SolarPlantViews(ListCreateAPIView):
    queryset = SolarPlant.objects
    filter_backends = [DjangoFilterBackend]


    def get_queryset(self):
        return self.queryset

    def create(self, request, *args, **kwargs):
        try:
            file = self.request.data['file']
            decoded_file = file.read().decode()
            io_string = io.StringIO(decoded_file)
            reader = csv.reader(io_string)
            count=0
            for row in reader:
                if count != 0:
                    instance = SolarPlant()
                    instance.plant_id = row[0]
                    instance.date = row[1]
                    instance.parameter = row[2]
                    instance.value = row[3]
                    instance.save()
                count = count + 1
            return Response(dict(message='Successfully created'), status=status.HTTP_200_OK)
        except:
            raise CustomAPIException("Something went wrong")



class PlantwiseGraph(View):
    def get(self,request):
        plant_id = request.GET.get('plant_id', '')
        start_date = request.GET.get('start_date', '')
        end_date = request.GET.get('end_date', '')
        if plant_id:
            queryset = SolarPlant.objects.filter(plant_id = int(plant_id))
        else:
            queryset = SolarPlant.objects.filter(plant_id=501)

        if start_date and end_date:
            date_range = [start_date, end_date]
            queryset = queryset.filter(date__range = date_range)
            
        dates = queryset.values_list("date",flat=True)
        dates = list(set(dates))

        data = []
        for date in dates:
            qs = queryset.filter(date = date)
            result = [date]


            for q in qs:
                result.append(q.value)
                
            data.append(result)

        context = {'data': data,'plant_id':plant_id}
        template = get_template('graph/parameter_graph.html')
        html_content = template.render(context)
        return HttpResponse(html_content)
