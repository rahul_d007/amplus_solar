### Description ###
AMPLUS_SOLAR

### Python: 3.8.x (64 bit)
### MySQL: 5.8.x
### Framework: Django 3.x, DjangoRestFramework 3.12.2

### How to Setup Locally ###

### Clone Repository:
$ git clone https://rahul_d007@bitbucket.org/rahul_d007/amplus_solar.git
 

### Create & Activate Virtualenv:
$ cd <app_root>
$ python3 -m venv ampenv
$ source ampenv/bin/activate

 

### Install Dependencies:
(ampenv) $ pip install -r requirements.txt


### Run DB migrations:
(ampenv) $ python manage.py makemigrations
(ampenv) $ python manage.py migrate

  

### Run the server:
(ampenv) $ python manage.py runserver


